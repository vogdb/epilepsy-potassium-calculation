### About

Model of Ictal and Interictal discharges during epilepsy. Please see
[this article](http://journals.plos.org/ploscompbiol/article?id=10.1371/journal.pcbi.1006186)
for details.

#### Install
You need [Python](https://www.python.org/downloads/) of version 3.6 or
higher. Also I highly recommend to use environments for work.
[Anaconda](https://www.anaconda.com/distribution/) is a good choice.
The required dependencies are: matplotlib, scipy, numpy, numba, pyyaml.
```
pip install matplotlib scipy numpy numba pyyaml
```

#### Run
- Adjust params as you need in `params_0d.py` and `params_2d.py`. 
- Execute `python epileptor/model_2d_full.py`. It gives 3 files as the result in `results` dir. These files have names: `<YYYY-mm-dd_HH.MM>_plain.npz`, `<YYYY-mm-dd_HH.MM>_points.npz`, `<YYYY-mm-dd_HH.MM>_params.yml` correspondingly, where `<YYYY-mm-dd_HH.MM>` is the date/time when your execution ended.
- To display results execute `python -d results/<YYYY-mm-dd_HH.MM> epileptor/display_2D.py`.
- Look up the display results in `media` directory. They all have `<YYYY-mm-dd_HH.MM>` part in common.

**Example**
Your execution successfully ended. You have 3 results:
`2019-01-27_17.14_plain.npz`, `2019-01-27_17.14_points.npz`, `2019-01-27_17.14_params.yml`.
`<YYYY-mm-dd_HH.MM>` is represented as `2019-01-27_17.14`.

from setuptools import setup, find_packages

with open('README.md') as f:
    README = f.read()

setup(
    name='epilepsy-potassium-calculation',
    install_requires=[
        'matplotlib',
        'scipy',
        'numpy>=1.15',
        'numba',
        'pyyaml',
    ],
    packages=find_packages(),
    version='0.0.1',
    author='',
    author_email='vozdhb@gmail.com',
    description='',
    long_description=README,
    long_description_content_type='text/markdown',
    url='https://gin.g-node.org/asanin/epilepsy-potassium-calculation.git',
    classifiers=[]
)

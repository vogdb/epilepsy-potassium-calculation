import matplotlib
import matplotlib.pyplot as plt
import numpy as np
from mpl_toolkits.axes_grid1.inset_locator import inset_axes
import matplotlib.patches as patches
from epileptor import util

font = {'size': 20}
matplotlib.rc('font', **font)

def plot_data(dt, state_list, title):
    state_dict = {}
    for state in state_list:
        state_dict[state.name] = state.values

    any_state = state_list[0]
    nt = len(any_state.values)
    t_end_ms = int(nt * dt)
    t_range_ms = np.linspace(0, t_end_ms, nt)

    sec_formatter = matplotlib.ticker.FuncFormatter(lambda ms, x: int(ms / 1000))

    def get_first_burst_x(values, l):
        avg = np.mean(values[l:l + 1000])
        x = np.where(values[l:] > (avg + 50))[0]
        if len(x) == 0:
            return None
        pad = 500
        return x[0] + l - pad

    def plot_UV(ax, l=0, r=len(t_range_ms)):
        if 'U' not in state_dict:  # or 'V' not in state_dict:
            return
        U = state_dict['U']
        # V = state_dict['V']

        ax.set_ylabel('mV')
        ax.set_title('a', loc='left')
        # ax.plot(t_ms[l:r], V[l:r], 'r', label='$V$')
        ax.plot(t_range_ms[l:r], U[l:r], '#009900', label='$U$')
        ax.legend()

        b_l = get_first_burst_x(U, l)
        if b_l is None:
            return
        len = 1500
        b_r = b_l + len
        rect = patches.Rectangle((b_l, -63), len, 90, linewidth=.5, edgecolor='r', fill=False)
        ax.add_patch(rect)
        ax_ins = inset_axes(ax, width='30%', height='70%', loc=2)
        ax_ins.plot(t_range_ms[b_l:b_r], U[b_l:b_r], '#009900')
        ax_ins.set_yticklabels([])
        ax_ins.set_xticklabels([])
        ax_ins.tick_params(left=False, bottom=False)
        plt.setp(ax_ins.spines.values(), color='r')

    def plot_uu(ax, l=0, r=len(t_range_ms)):
        if 'uu' not in state_dict:
            return
        uu = state_dict['uu']

        ax.set_title('b', loc='left')
        ax.set_ylabel('$u$(pA)')
        ax.plot(t_range_ms[l:r], uu[l:r], 'k')

    def plotNuXd(ax, l=0, r=len(t_range_ms)):
        if 'nu' not in state_dict or 'xD' not in state_dict:
            return
        nu = state_dict['nu']
        xD = state_dict['xD']

        ax.set_ylabel(r'$\nu$(Hz)')
        ax.set_title('d', loc='left')
        ax.plot(t_range_ms[l:r], nu[l:r], 'k', label=r'$\nu$')
        ax_r = ax.twinx()
        ax_r.plot(t_range_ms[l:r], xD[l:r], 'm', label='$x^D$')
        ax.set_xlabel('t(s)')
        ax_r.set_ylabel('$x^D$')
        ax.legend(loc='upper left')
        ax_r.legend(loc='upper right')

    def plotKNa(ax, l=0, r=len(t_range_ms)):
        if 'K' not in state_dict or 'Na' not in state_dict or 'INaKpump' not in state_dict:
            return
        K = state_dict['K']
        Na = state_dict['Na']
        INaKpump = state_dict['INaKpump']

        ax.set_ylabel('mM')
        ax.set_title('c', loc='left')
        ax.plot(t_range_ms[l:r], K[l:r], 'b', label='$[K]_o$')
        ax.plot(t_range_ms[l:r], Na[l:r], 'r', label='$[Na]_i$')
        ax_r = ax.twinx()
        ax_r.plot(t_range_ms[l:r], INaKpump[l:r] * 1000, 'C1', label='$I_{pump}$')
        ax_r.set_ylabel('mM/s')
        ax.legend(loc='upper left')
        ax_r.legend(loc='upper right')

    def plot_phi(ax, l=0, r=len(t_range_ms)):
        if 'phi' not in state_dict:
            return
        phi = state_dict['phi']
        ax.set_xlabel('t(s)')
        ax.set_ylabel(r'$\varphi$(Hz)')
        ax.set_title('e', loc='left')
        ax.plot(t_range_ms[l:r], phi[l:r])

    def has_phi():
        if 'phi' in state_dict:
            phi = state_dict['phi']
            if np.mean(phi) > 1e-6:
                return True
        return False

    def set_sec_formatter(axes):
        for ax in axes.flat:
            ax.xaxis.set_major_formatter(sec_formatter)

    def plot_with_zoom(left=50, right=100):
        left_nt = util.sec_to_dt(left, dt)
        right_nt = util.sec_to_dt(right, dt)
        do_zoom = right_nt <= len(t_range_ms)
        if not do_zoom:
            return

        nrows = 4
        if has_phi():
            nrows += 1
        fig, axes = plt.subplots(nrows, 2, sharex='col', sharey='row')
        set_sec_formatter(axes)

        plot_UV(axes[0, 0])
        plot_UV(axes[0, 1], left_nt, right_nt)
        plot_uu(axes[1, 0])
        plot_uu(axes[1, 1], left_nt, right_nt)
        plotKNa(axes[2, 0])
        plotKNa(axes[2, 1], left_nt, right_nt)
        plotNuXd(axes[3, 0])
        plotNuXd(axes[3, 1], left_nt, right_nt)
        if has_phi():
            plot_phi(axes[4, 0])
            plot_phi(axes[4, 1], left_nt, right_nt)

        # plt.subplots_adjust(wspace=0.25)
        plt.savefig('media/{}_zoom.eps'.format(title))

    def plot_no_zoom(left=0, right=int(t_end_ms / 1000)):
        left_nt = util.sec_to_dt(left, dt)
        right_nt = util.sec_to_dt(right, dt)

        nrows = 4
        if has_phi():
            nrows += 1
        fig, axes = plt.subplots(nrows, 1, sharex='all', figsize=(10,8))
        plt.subplots_adjust(hspace=0.6)
        set_sec_formatter(axes)

        plot_UV(axes[0], left_nt, right_nt)
        plot_uu(axes[1], left_nt, right_nt)
        plotKNa(axes[2], left_nt, right_nt)
        plotNuXd(axes[3], left_nt, right_nt)
        if has_phi():
            plot_phi(axes[4], left_nt, right_nt)

        # plt.tight_layout()
        plt.savefig('media/{}'.format(title))
        plt.gcf().clear()

    # plot_with_zoom(40, 70)
    # plot_no_zoom(10, 80)
    plot_no_zoom(0, 299)

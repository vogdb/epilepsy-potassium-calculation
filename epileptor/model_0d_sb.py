from __future__ import division
import matplotlib.pyplot as plt
from epileptor.params_0d import *
from epileptor.util import *

dt = 0.5  # ms
t_end = 10000  # ms 10s
t = np.linspace(0, t_end, int(t_end / dt) + 1)

# State variables
xD = np.zeros(len(t))
nu = np.zeros(len(t))
V = np.zeros(len(t))
uu = np.zeros(len(t))

# Initial conditions
xD[0] = 1
nu[0] = 0
V[0] = 0

# overwrite params
noiseAmpl = 40


# model
def ode_model(V, xD, noiseAmpl=noiseAmpl):
    # (5')
    nu = nuMax * np.maximum(sigmoid2((V - Vth) / gain), 0)

    # (3')
    I_stim = noiseAmpl / np.sqrt(dt) * np.random.normal()
    I_ex = gE * nu * xD
    I_in = -gIgE * gE * nu
    uu = I_ex + I_in + I_stim
    dVdt = 1 / C * (-gL * V + uu)

    # (4')
    dxDdt = 1 / tau_xD * (1 - xD) - dxDreset * nu * xD / 1000

    return dVdt, dxDdt, nu


# model integration with euler
for i in range(1, len(t)):
    dVdt, dxDdt, nu[i] = ode_model(
        V[i - 1], xD[i - 1]
    )
    V[i] = V[i - 1] + dt * dVdt
    xD[i] = xD[i - 1] + dt * dxDdt

# plot
figure = plt.figure(figsize=(10, 6))

subplot2 = figure.add_subplot(324)
subplot2.set_xlabel('t(ms)')
subplot2.set_ylabel('xD(Hz)')
subplot2.plot(t, xD, 'm')

subplot3 = figure.add_subplot(326)
subplot3.set_xlabel('t(ms)')
subplot3.set_ylabel('nu(Hz)')
subplot3.plot(t, nu, 'k')
subplot3.legend()

subplot1 = figure.add_subplot(322)
subplot1.set_xlabel('t(ms)')
subplot1.set_ylabel('V(mV)')
subplot1.plot(t, V, 'r')

subplot0 = figure.add_subplot(121)
subplot0.set_xlabel('V(mV)')
subplot0.set_ylabel('xD(Hz)')
subplot0.plot(V, xD, 'm')

V_min = np.amin(V)
V_max = np.amax(V)
xD_min = np.amin(xD)
xD_max = np.amax(xD)

# plot vector fields
V_range = np.linspace(V_min, V_max, 13)
xD_range = np.linspace(xD_min, xD_max, 16)
# grid where we plot vectors
V_grid, xD_grid = np.meshgrid(V_range, xD_range)
# calculate derivatives at grid points
dV_grid, dxD_grid, _ = ode_model(V_grid, xD_grid, noiseAmpl=0)
# normalize derivatives. their values will be encoded in colors
M = (np.hypot(dV_grid, dxD_grid))
M[M == 0] = 1.
dV_grid /= M
dxD_grid /= M
subplot0.quiver(V_grid, xD_grid, dV_grid, dxD_grid, M, pivot='mid')
subplot0.grid()

# plot V nullclines
V_margin = 0.1 * (V_max - V_min)
xD_margin = 0.1 * (xD_max - xD_min)
V_range = np.linspace(np.amin(V) - V_margin, V_max + V_margin, 100)
xD_range = np.linspace(xD_min - xD_margin, xD_max + xD_margin, 50)
V_grid, xD_grid = np.meshgrid(V_range, xD_range)
dV_grid, dxD_grid, _ = ode_model(V_grid, xD_grid, noiseAmpl=0)
subplot0.contour(V_grid, xD_grid, dV_grid, levels=[0], linewidths=3, colors='black', zorder=3)

plt.show()

from __future__ import division

import time

from epileptor.phi_2d import PhiSolver
from epileptor.params_0d import *
from epileptor.params_2d import *
from epileptor.state_recorder import StateRecorder
from abc import ABC, abstractmethod

np.set_printoptions(precision=3)


class NoiseGenerator(ABC):
    @abstractmethod
    def generate(self, y_nh, x_nh):
        pass


class UniNoiseGenerator(NoiseGenerator):
    def generate(self, y_nh, x_nh):
        return np.random.normal()


class RndNoiseGenerator(NoiseGenerator):
    def generate(self, y_nh, x_nh):
        return np.random.normal(size=(y_nh, x_nh))


def split_h_circle(x_nh, y_nh, dh):
    in_x_list, in_y_list = [], []
    out_x_list, out_y_list = [], []
    for x in range(x_nh):
        for y in range(y_nh):
            out_circle = np.sqrt((x - x_nh / 2) ** 2 + (y - y_nh / 2) ** 2) > Radius / dh + 1e-6
            if out_circle:
                out_x_list.append(x)
                out_y_list.append(y)
            else:
                in_x_list.append(x)
                in_y_list.append(y)
    # we swap here x and y as np indexes are reversed, e.g. [y, x]
    return np.s_[in_y_list, in_x_list], np.s_[out_y_list, out_x_list]


def laplacian(f, dh):
    bottom = f[0:-2, 1:-1]
    left = f[1:-1, 0:-2]
    top = f[2:, 1:-1]
    right = f[1:-1, 2:]
    center = f[1:-1, 1:-1]
    return (bottom + left + top + right - 4 * center) / dh ** 2


def boundary_conditions(f):
    # Neumann BCs on all sides
    f[0, :] = f[1, :]
    f[:, 0] = f[:, 1]
    f[-1, :] = f[-2, :]
    f[:, -1] = f[:, -2]

    f[0, 0] = f[1, 1]
    f[-1, 0] = f[-2, 1]
    f[0, -1] = f[1, -2]
    f[-1, -1] = f[-2, -2]


def solve():
    t = np.linspace(0, t_end, int(t_end / dt) + 1)
    nt = len(t)

    recorder = StateRecorder(nt, extract_params_to_dict())
    # init state
    K = np.ones((y_nh, x_nh)) * K0
    U = np.ones((y_nh, x_nh)) * U0
    w = np.zeros((y_nh, x_nh))
    Na = np.ones((y_nh, x_nh)) * Na0
    xD = np.ones((y_nh, x_nh))
    V = np.zeros((y_nh, x_nh))

    for dt_i in range(nt - 1):
        dKdt, dNadt, dVdt, dUdt, dwdt, dxDdt, uu, nu, INaKpump, phi = ode_step(K, Na, V, U, w, xD)

        K = K_step(K, dKdt)
        U += dt * dUdt
        w += dt * dwdt
        Na += dt * dNadt
        xD += dt * dxDdt
        V += dt * dVdt
        # Reset
        Uspike = U >= Uth
        U[Uspike] = Ureset
        w[Uspike] = w[Uspike] + delta_w

        recorder.record_plain(dt_i, 'K', K)
        # recorder.record_plain(dt_i, 'V', V)
        recorder.record_points(dt_i, K, Na, INaKpump, V, U, xD, uu, nu, phi)
    return recorder


def ode_step(K, Na, V, U, w, xD):
    # (5)
    nu_x = np.clip(-2 * (V - Vth) / gain, -20, 20)
    nu = nuMax * np.maximum((2 / (1 + np.exp(nu_x)) - 1), 0)
    if cut_idx:
        nu[cut_idx] = 0

    # (7)
    VK = 26.6 * np.log(K / 130)

    if is_syn_calc:
        phi = phi_solver.ode_step(nu, cut_idx)
        if cut_idx:
            phi[cut_idx] = 0
        phi_or_nu = phi
    else:
        phi = np.zeros(nu.shape)
        phi_or_nu = nu

    # (6)
    I_stim = (VK - VK0) * gKleak + noiseAmpl / (np.sqrt(dt)) * noise_generator.generate(y_nh, x_nh)
    uu = I_stim + phi_or_nu * gE * (xD - gIgE)

    # (8) Na-K pump, [Wei 2014]
    INaKpump = roPump / ((1 + np.exp(3.5 - K)) * (1 + np.exp((25 - Na) / 3)))

    # (9)
    dUdt = 1 / CU * (gU * (U - U1) * (U - U2) + uu - w + Ia)
    dwdt = -w / tau_w

    # (1)
    if is_diff_calc:
        ddK = D_K * laplacian(K, dh)
        dKdt = ddK + 1 / tauK * (Kbath - K[1:-1, 1:-1]) - 2 * roKroNa * INaKpump[1:-1, 1:-1] \
               + dKreset * phi_or_nu[1:-1, 1:-1] / 1000
    else:
        dKdt = 1 / tauK * (Kbath - K) - 2 * roKroNa * INaKpump + dKreset * phi_or_nu / 1000
    # (2)
    dNadt = 1 / tauNa * (Na0 - Na) - 3 * INaKpump + dNaReset * phi_or_nu / 1000
    # (4)
    dxDdt = 1 / tau_xD * (1 - xD) - dxDreset * phi_or_nu * xD / 1000
    # (3)
    dVdt = 1 / C * (-gL * V + uu)

    return dKdt, dNadt, dVdt, dUdt, dwdt, dxDdt, uu, nu, INaKpump, phi


def K_step(K, dKdt):
    if is_diff_calc:
        K[1:-1, 1:-1] += dt * dKdt
        boundary_conditions(K)
    else:
        K += dt * dKdt
    return K


def extract_params_to_dict():
    import epileptor.params_2d
    module_prop_names = dir(epileptor.params_2d)
    param_names = [prop_name for prop_name in module_prop_names if not prop_name.startswith('__')]
    return {param_name: epileptor.params_2d.__dict__.get(param_name) for param_name in param_names}


if __name__ == '__main__':
    np.random.seed(66)
    print('calculation has started')
    if is_noise_uni:
        noise_generator = UniNoiseGenerator()
    else:
        noise_generator = RndNoiseGenerator()
    # Some params become spatial
    gE = np.ones((y_nh, x_nh)) * gE
    if Radius > 0:
        _, out_circle_idx = split_h_circle(x_nh, y_nh, dh)
        gE[out_circle_idx] = gE_normal
    phi_solver = PhiSolver(x_nh, y_nh, PhiSolver.calc_sigma(dh, lamb))

    start_time = time.time()
    recorder = solve()
    end_time = time.time()
    print('execution time: {}s'.format(end_time - start_time))

    print('saving results')
    results_filename = recorder.save()
    print('results have been saved to {}'.format(results_filename))

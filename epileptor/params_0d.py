import numpy as np

# params
CU = 1050  # pF # 1000
gU = 1.5  # nS/mV # 1.5
Uth = 25  # mV
Ureset = -40  # mV
dKreset = 0.02  # mM
gKleak = 0.5
dNaReset = 0.03  # mM
gE = 5.0  # mV * s
gIgE = 0.5  #
tauK = 100000  # ms 100sec
roPump = 0.0002  # mM/ms
# DB = 1000
tau_xD = 2000  # 2sec
dxDreset = 0.01
gain = 20  # mV
Vth = 25  # mV
noiseAmpl = 25  # pA
tauNa = 20000  # ms 20s
roKroNa = 10
K0 = 3  # mM
Kbath = 8.5  # mM
VK0 = 26.6 * np.log(K0 / 130)
Na0 = 10  # mM
gL = 1  # nS
C = 10  # pF
U1 = -60  # mV
U2 = -40  # mV
U0 = -70  # mV
nuMax = 100  # Hz
Ia = 116  # pA
tau_w = 200  # ms
delta_w = 100  # pA

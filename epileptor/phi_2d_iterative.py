# for boosting of phi_2d computation this module does phi_2d.iterative_step in raw cycles and applies JIT Numba.
# Pythran gives the same acceleration as Numba hence it is not used as it requries additional compilation step.
from numba import jit
import numpy as np


# pythran export calculate(float64, float64[][], float64, float64[][])
@jit
# def calculate(sigma, nu, l2_target, phi_init, cut_idx=None):
def calculate(sigma, nu, l2_target, phi_init):
    l2_norm = 1
    iterations = 0
    b = nu * sigma
    # initial guess
    phi = phi_init.copy()
    phi_prev = np.empty_like(phi)

    m, n = nu.shape
    sigma4 = 1. / (4. + sigma)

    while l2_norm > l2_target:
        for i in range(0, m):
            for j in range(0, n):
                phi_prev[i, j] = phi[i, j]
        # omp parallel for schedule(guided)
        for i in range(1, m - 1):
            # i - rows
            for j in range(1, n - 1):
                # j - columns
                phi[i, j] = sigma4 * (
                        phi_prev[i - 1, j] + phi_prev[i + 1, j] + phi_prev[i, j - 1] + phi_prev[i, j + 1] +
                        b[i, j]
                )

        # if cut_idx:
        #     phi[cut_idx] = 0

        l2_norm_num = 0
        l2_norm_div = 0
        # omp parallel for schedule(guided)
        for i in range(1, m-1):
            # i - rows
            for j in range(1, n-1):
                l2_norm_num += (phi[i, j] - phi_prev[i, j]) ** 2
                l2_norm_div += phi_prev[i, j] ** 2
        l2_norm_div = max(l2_norm_div, 1e-6)
        l2_norm = l2_norm_num ** 0.5 / l2_norm_div ** 0.5

        iterations += 1

    # Neumann boundary
    for i in range(0, m):
        phi[i, 0] = phi[i, 1]
        phi[i, n - 1] = phi[i, n - 2]
    for j in range(0, n):
        phi[0, j] = phi[1, j]
        phi[m - 1, j] = phi[m - 2, j]
    phi[0, 0] = phi[1, 1]
    phi[-1, 0] = phi[-2, 1]
    phi[0, -1] = phi[1, -2]
    phi[-1, -1] = phi[-2, -2]

    return phi, iterations

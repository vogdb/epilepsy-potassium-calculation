from __future__ import division

from epileptor import display_0d
from epileptor.util import *
from epileptor.params_0d import *
from epileptor.state import State
from epileptor.state_recorder import StateRecorder


def ode_step(K, Na, V, U, xD):
    # (5)
    nu = nuMax * np.maximum(sigmoid2((V - Vth) / gain), 0)

    # (7)
    VK = 26.6 * np.log(K / 130)

    # (6)
    I_stim = (VK - VK0) * gKleak + noiseAmpl / np.sqrt(dt) * np.random.normal()
    I_ex = gE * nu * xD
    I_in = -gIgE * gE * nu
    # Depolarization block validation
    # if I_stim + I_ex > DB:
    #     I_in = 0
    uu = I_stim + I_ex + I_in

    # (8) Na-K pump, [Wei 2014]
    INaKpump = roPump / ((1 + dexp(3.5 - K)) * (1 + dexp((25 - Na) / 3)))

    # (9)
    dUdt = 1 / CU * (gU * (U - U1) * (U - U2) + uu)

    # (1)
    dKdt = 1 / tauK * (Kbath - K) - 2 * roKroNa * INaKpump + dKreset * nu / 1000
    # (2)
    dNadt = 1 / tauNa * (Na0 - Na) - 3 * INaKpump + dNaReset * nu / 1000
    # (4)
    dxDdt = 1 / tau_xD * (1 - xD) - dxDreset * nu * xD / 1000
    # (3)
    dVdt = 1 / C * (-gL * V + uu)

    return dKdt, dNadt, dVdt, dUdt, dxDdt, uu, nu, INaKpump


dt = 0.5  # ms
t_end = 100000  # ms 600s
t = np.linspace(0, t_end, int(t_end / dt) + 1)
recorder = StateRecorder(dt, t_end)

# init state
U = U0
K = K0
Na = Na0
xD = 1
nu = 0
V = 0

# model integration with euler
for n in range(1, len(t)):
    dKdt, dNadt, dVdt, dUdt, dxDdt, uu, nu, INaKpump = ode_step(K, Na, V, U, xD)
    K = max(0, K + dt * dKdt)
    Na = Na + dt * dNadt
    V = V + dt * dVdt
    U = U + dt * dUdt
    xD = xD + dt * dxDdt
    # Reset
    if U >= Uth:
        U = Ureset

    recorder.record(n, K, Na, INaKpump, V, U, xD, uu, nu)

state_list = [State(k, v) for k, v in recorder.get_dict().items()]
display_0d.plot_data(recorder.get_dt(), state_list, 'single')

import numpy as np


class State:

    def __init__(self, name, values):
        self.name = name
        self.values = np.array(values)
        self.min = np.min(self.values)
        self.max = np.max(self.values)

# import numpy as np
# overwrite 1d params
dKreset = 0.04
roKroNa = 20
gKleak = 1
Kbath = 7

# new params
D_K = 4e-7  # mm^2/ms
gE_normal = 1

y_h = 6  # mm
y_nh = 80
dh = y_h / y_nh
x_nh = 80
x_h = x_nh * dh  # mm
Radius = 0.3  # mm
dt = 1.0
t_end = 300000  # 1000 is 1 second
lamb = 0.385  # mm
is_diff_calc = True
is_syn_calc = False
is_noise_uni = True
cut_idx = None  # np.s_[:, int(2 * x_nh / 3)]

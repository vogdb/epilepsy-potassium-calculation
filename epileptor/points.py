import numpy as np


class Point():
    def __init__(self, x, y):
        self.x = x
        self.y = y


class Points:

    def __init__(self, *point_list):
        self._point_list = list(point_list)
        self._build_np_index()

    def _build_np_index(self):
        x_coords = []
        y_coords = []
        for point in self._point_list:
            x_coords.append(point.x)
            y_coords.append(point.y)
        self._np_index = np.s_[y_coords, x_coords]

    def as_np_index(self):
        return self._np_index

    def get_point_index(self, point):
        for i, point_i in enumerate(self._point_list):
            if point_i.x == point.x and point_i.y == point.y:
                return i
        raise RuntimeError('Invalid Point in Points')

    def get(self, idx):
        return self._point_list[idx]

    def len(self):
        return len(self._point_list)

    def __getitem__(self, index):
        return self.get(index)

    def __len__(self):
        return self.len()

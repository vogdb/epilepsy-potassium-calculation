import numpy as np
from epileptor.phi_2d_iterative import calculate


class PhiSolver:

    @staticmethod
    def calc_sigma(dh, lamb):
        # h = 3 mm => sigma = 0.0225
        # h = 1 mm => sigma = 0.0025
        sigma = dh ** 2 / lamb ** 2  # unitless
        return sigma

    def __init__(self, x_nh, y_nh, sigma):
        self.phi_prev = np.zeros((y_nh, x_nh))
        # make on item nonzero so we don't divide on zero further
        self.phi_prev[1, 1] = 1
        self.x_nh = x_nh
        self.y_nh = y_nh
        self.sigma = sigma
        self.A = self.inverse_matrix_coef_explicit()

    def ode_step(self, nu, cut_idx):
        # self.phi_prev, _ = self.iterative_step(nu, 1e-8, self.phi_prev)
        self.phi_prev, _ = calculate(self.sigma, nu, 1e-8, self.phi_prev)
        return self.phi_prev
        # return self.inverse_step(nu)

    def inverse_step(self, nu):
        '''
        works faster for lambda > 0.3
        '''
        b = self.generate_rhs(nu)
        phi_in_1d = np.linalg.solve(self.A, b)
        return self.map1D_to_2D(phi_in_1d)

    def inverse_matrix_coef_explicit(self):
        """ Generate implicit matrix for Neumann 0 boundary conditions.

        Returns:
        -------
        A: 2D array of floats
            Matrix of implicit 2D equation
        """
        x_nh_in, y_nh_in = self.x_nh - 2, self.y_nh - 2
        A = np.zeros((x_nh_in * y_nh_in, x_nh_in * y_nh_in))

        # j - y direction
        for j in range(y_nh_in):
            # i - x direction
            for i in range(x_nh_in):
                I = j * x_nh_in + i  # row index
                south, west, east, north = I - x_nh_in, I - 1, I + 1, I + x_nh_in

                # Corners
                if i == 0 and j == 0:  # Bottom left corner
                    A[I, I] = self.sigma + 2  # Set diagonal
                    A[I, east] = -1  # fetch i+1
                    A[I, north] = -1  # fetch j+1

                elif i == x_nh_in - 1 and j == 0:  # Bottom right corner
                    A[I, I] = self.sigma + 2  # Set diagonal
                    A[I, west] = -1  # Fetch i-1
                    A[I, north] = -1  # fetch j+1

                elif i == 0 and j == y_nh_in - 1:  # Top left corner
                    A[I, I] = self.sigma + 2  # Set diagonal
                    A[I, east] = -1  # fetch i+1
                    A[I, south] = -1  # fetch j-1

                elif i == x_nh_in - 1 and j == y_nh_in - 1:  # Top right corner
                    A[I, I] = self.sigma + 2  # Set diagonal
                    A[I, west] = -1  # Fetch i-1
                    A[I, south] = -1  # fetch j-1

                # Sides
                elif i == 0:  # Left boundary
                    A[I, I] = self.sigma + 3  # Set diagonal
                    A[I, east] = -1  # fetch i+1
                    A[I, north] = -1  # fetch j+1
                    A[I, south] = -1  # fetch j-1

                elif i == x_nh_in - 1:  # Right boundary
                    A[I, I] = self.sigma + 3  # Set diagonal
                    A[I, west] = -1  # Fetch i-1
                    A[I, north] = -1  # fetch j+1
                    A[I, south] = -1  # fetch j-1

                elif j == 0:  # Bottom boundary
                    A[I, I] = self.sigma + 3  # Set diagonal
                    A[I, east] = -1  # fetch i+1
                    A[I, west] = -1  # fetch i-1
                    A[I, north] = -1  # fetch j+1

                elif j == y_nh_in - 1:  # Top boundary
                    A[I, I] = self.sigma + 3  # Set diagonal
                    A[I, east] = -1  # fetch i+1
                    A[I, west] = -1  # fetch i-1
                    A[I, south] = -1  # fetch j-1

                # Interior points
                else:
                    A[I, I] = self.sigma + 4  # Set diagonal
                    A[I, east] = -1  # fetch i+1
                    A[I, west] = -1  # fetch i-1
                    A[I, north] = -1  # fetch j+1
                    A[I, south] = -1  # fetch j-1

                I += 1  # Jump to next row of the matrix!

        return A

    def inverse_matrix_coef_implicit(self):
        def kth_diag_indices(a, k):
            rows, cols = np.diag_indices_from(a)
            if k < 0:
                return rows[-k:], cols[:k]
            elif k > 0:
                return rows[:-k], cols[k:]
            else:
                return rows, cols

        def calc_row_number(i, j):
            return j * x_nh_in + i

        x_nh_in, y_nh_in = self.x_nh - 2, self.y_nh - 2

        A_size = x_nh_in * y_nh_in
        A = np.diag([self.sigma + 4] * A_size)
        east_idx = kth_diag_indices(A, 1)
        west_idx = kth_diag_indices(A, -1)
        north_idx = kth_diag_indices(A, x_nh_in)
        south_idx = kth_diag_indices(A, -x_nh_in)
        A[east_idx] = -1
        A[west_idx] = -1
        A[north_idx] = -1
        A[south_idx] = -1

        # Left boundary
        left_border = calc_row_number(0, np.arange(1, y_nh_in))
        A[left_border, left_border - 1] = 0
        A[left_border, left_border] -= 1
        # Right boundary
        right_border = calc_row_number(x_nh_in - 1, np.arange(0, y_nh_in - 1))
        A[right_border, right_border + 1] = 0
        A[right_border, right_border] -= 1
        # Top boundary
        top_border = calc_row_number(np.arange(0, x_nh_in - 1), y_nh_in - 1)
        A[top_border, top_border] -= 1
        # Bottom boundary
        bottom_border = calc_row_number(np.arange(1, x_nh_in), 0)
        A[bottom_border, bottom_border] -= 1

        # Bottom left corner
        bl_corner = calc_row_number(0, 0)
        A[bl_corner, bl_corner] -= 2
        # Top right corner
        tr_corner = calc_row_number(x_nh_in - 1, y_nh_in - 1)
        A[tr_corner, tr_corner] -= 2

        return A

    def generate_rhs(self, phi):
        ''' Generates right-hand side for Neumann 0 boundary conditions. Assumes x=y.

            Parameters:
            -----------
            phi  : array of float
                Phi in current time step

            Returns:
            -------
            RHS  : array of float
                Right hand side of 2D implicit equation
        '''
        return self.sigma * phi[1:-1, 1:-1].flatten()

    def map1D_to_2D(self, phi_in_1d):
        ''' Takes solution of linear system, stored in 1D, and puts them in a 2D array with the Neumann 0 BCs

        Parameters:
        ----------
            phi_in_1d : array of floats
                solution of linear system

        Returns:
        -------
            phi: 2D array of float
                Phi stored in 2D array with BCs
        '''
        phi = np.zeros((self.y_nh, self.x_nh))

        phi[1:-1, 1:-1] = phi_in_1d.reshape((self.y_nh - 2, self.x_nh - 2))
        # Neumann BC
        phi[0, :] = phi[1, :]
        phi[:, 0] = phi[:, 1]
        phi[-1, :] = phi[-2, :]
        phi[:, -1] = phi[:, -2]

        return phi

    def iterative_step(self, nu, l2_target, phi_init=None):
        '''Performs Jacobi relaxation

        Parameters:
        ----------
        nu : 2D array of floats
            Source term
        l2_target: float
            Target difference between two consecutive iterates

        Returns:
        -------
        p: 2D array of float
            Distribution after relaxation
        '''

        l2_norm = 1
        iterations = 0
        b = nu * self.sigma
        # initial guess
        if phi_init is None:
            phi = b.copy()
        else:
            phi = phi_init

        while l2_norm > l2_target:
            p_previous = phi.copy()
            phi[1:-1, 1:-1] = (1. / (4. + self.sigma)) * (
                    p_previous[1:-1, 2:] + p_previous[1:-1, :-2] + p_previous[2:, 1:-1] + p_previous[:-2, 1:-1]
                    + b[1:-1, 1:-1]
            )
            phi[0, :] = phi[1, :]
            phi[:, 0] = phi[:, 1]
            phi[-1, :] = phi[-2, :]
            phi[:, -1] = phi[:, -2]

            phi[0, 0] = phi[1, 1]
            phi[-1, 0] = phi[-2, 1]
            phi[0, -1] = phi[1, -2]
            phi[-1, -1] = phi[-2, -2]

            # l2_norm = np.linalg.norm(p - p_previous) / np.maximum(np.linalg.norm(p_previous), 1e-8)
            l2_norm = np.sqrt(np.sum((phi - p_previous) ** 2)) / np.maximum(np.sqrt(np.sum(p_previous ** 2)), 1e-8)
            iterations += 1

        return phi, iterations

    def iterative_inverse_step(self, nu, l2_target, phi_init=None):
        def mtrx():
            A_size = nh
            A = np.diag([self.sigma + 2] * A_size)
            A += np.diag([-1] * (A_size - 1), -1)
            A += np.diag([-1] * (A_size - 1), 1)
            return A

        def generate_rhs_dx(b, pre):
            # b + d^2_phi/d_x^2
            rhs = b[:, 1:-1] + pre[:, 2:] + pre[:, :-2] - 2 * pre[:, 1:-1]
            return rhs

        def generate_rhs_dy(b, pre):
            # b + d^2_phi/d_y^2
            rhs = b[1:-1, :] + pre[2:, :] + pre[:-2, :] - 2 * pre[1:-1, :]
            return rhs

        if self.x_nh != self.y_nh:
            raise RuntimeError('for `iterative_inverse_step` x_nh and y_nh must be equal')
        nh = self.x_nh
        l2_norm = 1
        iterations = 0
        b = nu * self.sigma
        # initial guess
        if phi_init is None:
            phi = b.copy()
        else:
            phi = phi_init

        A = mtrx()

        while l2_norm > l2_target:
            phi_prev = phi.copy()
            if iterations % 2 == 0:
                # over y
                rhs = generate_rhs_dx(b, phi_prev)
                phi[:, 1:-1] = np.linalg.solve(A, rhs)
            else:
                # over x
                rhs = generate_rhs_dy(b, phi_prev)
                phi[1:-1, :] = np.linalg.solve(A, rhs.T).T
            phi[0, :] = phi[1, :]
            phi[:, 0] = phi[:, 1]
            phi[-1, :] = phi[-2, :]
            phi[:, -1] = phi[:, -2]

            l2_norm = np.sqrt(np.sum((phi - phi_prev) ** 2)) / np.maximum(np.sqrt(np.sum(phi_prev ** 2)), 1e-8)
            iterations += 1
            if iterations > 1000:
                print('Warning, PhiSolver.iterative_inverse_step didn\'t converge')
                break

        return phi, iterations


if __name__ == "__main__":
    # Just local testing of the module.
    from epileptor import params_2d as params
    from mpl_toolkits.mplot3d import Axes3D
    import matplotlib.pyplot as plt
    import timeit

    timeit_num = 10
    x_nh = 40
    y_nh = 40
    y_h = x_h = 3
    dh = y_h / y_nh
    sigma = PhiSolver.calc_sigma(dh, params.lamb)
    solver = PhiSolver(x_nh, y_nh, sigma)

    A_explicit = solver.inverse_matrix_coef_explicit()
    A_implicit = solver.inverse_matrix_coef_implicit()
    assert np.allclose(A_explicit, A_implicit), 'inverse matrix coefficients inconsistent constructing'

    # init nu
    nu = np.random.normal(size=(y_nh, x_nh))
    for y in range(y_nh):
        for x in range(x_nh):
            in_circle = np.sqrt((x - x_nh // 2) ** 2 + (y - y_nh // 2) ** 2) <= params.Radius / dh + 1e-6
            if in_circle:
                nu[y][x] = 100 + np.random.normal()

    # inverse
    phi_inverse = solver.inverse_step(nu)
    print('inverse time: {}'.format(
        timeit.timeit('solver.inverse_step(nu)', number=timeit_num, globals=globals())
    ))

    # iterative
    phi_iterative, _ = solver.iterative_step(nu, 1e-8)
    print('iterative time: {}'.format(
        timeit.timeit('solver.iterative_step(nu, 1e-8)', number=timeit_num, globals=globals())
    ))

    # iterative plain
    phi_iterative_plain, _ = calculate(sigma, nu, 1e-8, np.ones(nu.shape))
    print('iterative plain time: {}'.format(
        timeit.timeit('calculate(sigma, nu, 1e-8, np.ones(nu.shape))', number=timeit_num, globals=globals())
    ))

    print(np.allclose(phi_inverse, phi_iterative_plain, atol=1e-3))
    # idx = 10
    # print(np.hstack((phi_inverse[:, idx][:, np.newaxis], phi_iterative[:, idx][:, np.newaxis], phi_iterative_plain[:, idx][:, np.newaxis])))
    # print(np.hstack((phi_inverse[idx, :][:, np.newaxis], phi_iterative[idx, :][:, np.newaxis], phi_iterative_plain[idx, :][:, np.newaxis])))
    print(np.sum(phi_inverse), np.sum(phi_iterative), np.sum(phi_iterative_plain))

    x = np.linspace(0, x_h, x_nh)
    y = np.linspace(0, y_h, y_nh)
    x, y = np.meshgrid(x, y)
    fig = plt.figure(figsize=(14, 10))

    ax_nu = plt.subplot(221, projection='3d')
    ax_nu.plot_surface(x, y, nu, cmap='plasma')
    # ax_nu.axis([x_h / 2 - 2 * x_h, x_h / 2 + 2 * x_h, 0, y_h])
    ax_nu.set_title('Nu')

    ax_phi1 = plt.subplot(222, projection='3d')
    ax_phi1.plot_surface(x, y, phi_inverse, cmap='plasma')
    # ax_phi1.axis([x_h / 2 - 2 * x_h, x_h / 2 + 2 * x_h, 0, y_h])
    ax_phi1.set_title('Phi inverse')

    ax_phi2 = plt.subplot(223, projection='3d')
    ax_phi2.plot_surface(x, y, phi_iterative, cmap='plasma')
    # ax_phi2.axis([x_h / 2 - 2 * x_h, x_h / 2 + 2 * x_h, 0, y_h])
    ax_phi2.set_title('Phi iterative numpy')

    ax_phi3 = plt.subplot(224, projection='3d')
    ax_phi3.plot_surface(x, y, phi_iterative_plain, cmap='plasma')
    ax_phi3.set_title('Phi iterative plain')

    plt.show()

import numpy as np
from scipy.signal import find_peaks


def dexp(x):
    if x < -20:
        return 0
    elif x > 20:
        return np.exp(20)
    else:
        return np.exp(x)


vexp = np.vectorize(dexp)


def sigmoid2(x):
    return 2 / (1 + vexp(-2 * x)) - 1


def sigmoid3(x):
    if x < 0:
        return 0
    elif x < 1:
        return x
    else:
        return 1


def sec_to_dt(sec, dt):
    return int(sec * 1000.0 / dt)


def dt_to_sec(t_dt, dt):
    ms = t_dt * dt
    return int(ms / 1000.0)


def get_peak_times(state_values, distance_dt, threshold=None):
    t_n, y_nh, x_nh = state_values.shape
    center_point = np.s_[:, y_nh // 2, x_nh // 2]
    center_values = state_values[center_point]
    if threshold is None:
        max = np.max(center_values)
        threshold = max - max / 10
    peak_times, _ = find_peaks(center_values, height=threshold, distance=distance_dt)
    return peak_times

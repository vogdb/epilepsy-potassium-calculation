import numpy as np
import matplotlib.pyplot as plt
from epileptor.state_recorder import StateRecorder

uni_filepath = './results/xnh4_ynh100/2019-02-13_14.42_diff_uninoise'
rnd_filepath = './results/xnh4_ynh100/2019-02-13_14.49_diff_rndnoise'


def plotK(filepath, color, title):
    _, plain_data, _ = StateRecorder.load(filepath)
    K = plain_data['K']
    t = K.shape[0]

    x_coord = 2
    y_coord = 50
    t_peak = np.argmax(K[:, y_coord, x_coord], axis=0)
    print(t_peak, K[t_peak, y_coord, x_coord])
    t_buffer = 20000
    ax = plt.gca()
    ax.plot(K[(t_peak - t_buffer): (t_peak + t_buffer), y_coord, x_coord], color, label=title)


fig = plt.figure(figsize=(10, 8))

plotK(uni_filepath, 'r', 'uninoise')
plotK(rnd_filepath, 'b', 'rndnoise')

plt.gca().legend()
plt.show()

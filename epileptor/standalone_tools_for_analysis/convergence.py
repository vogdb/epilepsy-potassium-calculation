from __future__ import division
import numpy as np
from matplotlib import pyplot as plt
from epileptor import params_2d as params


def convergence_order(f3_f2, f2_f1, r):
    return np.log(f3_f2 / f2_f1) / np.log(r)


def l1_diff(coarse, fine, dh):
    N_coarse = len(coarse)
    N_fine = len(fine)
    ratio = int(np.ceil(N_fine / N_coarse))
    diff = dh * np.sum(np.abs(coarse - fine[::ratio]))
    return diff


def profile_convergence(dh_list, nh_list, matrix_list):
    profile_dict = dict(
        profile_diag_main=lambda n: np.s_[range(n), range(n)],
        profile_diag_anti=lambda n: np.s_[range(n - 1, -1, -1), range(n)]
    )

    for profile_name, profile in profile_dict.items():
        test_profile_list = [matrix[profile(matrix.shape[0])] for matrix in matrix_list]
        print('{} compare to dh: {}, nh: {}'.format(profile_name, dh_list[-1], nh_list[-1]))
        for i in range(len(test_profile_list) - 1):
            test_profile = test_profile_list[i]
            test_diff = l1_diff(test_profile, test_profile_list[-1], dh_list[i])
            print('dh: {}, nh: {}, diff: {}'.format(dh_list[i], nh_list[i], test_diff))

        f2_f1 = l1_diff(test_profile_list[0], test_profile_list[1], dh_list[1])
        # Calculate f3 - f2.
        f3_f2 = l1_diff(test_profile_list[1], test_profile_list[2], dh_list[2])
        p = convergence_order(f3_f2, f2_f1, 2)
        print('Observed order of convergence: p = {:.3f}'.format(p))


# h = 1
# test_nh = [10, 20, 40]
# test_dh = [h / nh for nh in test_nh]
# test_matrices = [5 * np.ones((nh, nh)) + np.random.normal() for nh in test_nh]
# profile_convergence(
#     test_dh,
#     test_nh,
#     test_matrices,
# )

def dh_convergence():
    nh_list = [10, 20, 40, 80]
    # times of K wave
    t_len = 35
    t_start_list = [37, 45, 40, 40]
    dh_list = []
    K_list = []
    dh_filename = 'model_2d_diff_nh{}_120s_dt1ms.npz'
    for i in range(len(nh_list)):
        nh = nh_list[i]
        data = np.load(dh_filename.format(nh))
        K = data['K']
        dh = params.h / nh
        dh_list.append(dh)
        t_start = t_start_list[i] * 1000
        t_end = t_start + t_len * 1000
        # integral_K = np.sum(K[t_start:t_end, :, :]) * dh ** 2 / (t_end - t_start)
        h_left = int(nh / 2 - np.ceil(params.Radius / dh)) - 1
        h_right = int(nh / 2 + np.ceil(params.Radius / dh)) + 1
        integral_K = np.sum(K[t_start:t_end, h_left:h_right, h_left:h_right]) * dh ** 2 / (t_end - t_start)
        print(K.shape, dh, integral_K)
        K_list.append(integral_K)

    plt.xlabel('dh')
    plt.ylabel('Difference')
    K_list = np.array(K_list)
    K_diff = np.abs(K_list - K_list[-1])
    plt.loglog(dh_list[:-1], K_diff[:-1], color='C0', linestyle='--', marker='o')
    # plt.axis('equal')
    plt.show()


# dh_convergence()

# nh_K[t_start:t_end, :, :] / (t_end - t_start)
# (120000, 10, 10) 0.3 38.03173083940235
# (120000, 20, 20) 0.15 38.91234810074353
# (120000, 40, 40) 0.075 38.57607671377342
# (120000, 80, 80) 0.0375 38.6374112625828

# nh_K[t_start:t_end, h_left:h_right, h_left:h_right]
# (120000, 10, 10) 0.3 251573.59206220263
# (120000, 20, 20) 0.15 169639.09648501076
# (120000, 40, 40) 0.075 132841.44241614133
# (120000, 80, 80) 0.0375 116086.6321028034

# nh_K[t_start:t_end, h_left:h_right, h_left:h_right] / (t_end - t_start)
# (120000, 10, 10) 0.3 7.187816916062932
# (120000, 20, 20) 0.15 4.846831328143164
# (120000, 40, 40) 0.075 3.7954697833183237
# (120000, 80, 80) 0.0375 3.3167609172229544


def dt_convergence():
    dt_list = [2.0, 1.0, .5, .25]
    # times of K wave
    t_len = 60  # s
    K_list = []
    dt_filename = 'dt_inf/model_2d_diff_nh40_120s_dt{}ms.npz'
    for i in range(len(dt_list)):
        dt = dt_list[i]
        data = np.load(dt_filename.format(dt))
        K = data['K']
        t_start = int(20 * 1000 / dt)
        t_end = int(t_start + t_len * 1000 / dt)
        integral_K = np.sum(K[t_start:t_end, :, :]) * dt / (1000 * t_len)
        # h_left = int(params.nh / 2 - np.ceil(params.Radius / params.dh)) - 1
        # h_right = int(params.nh / 2 + np.ceil(params.Radius / params.dh)) + 1
        # integral_K = np.sum(K[t_start:t_end, h_left:h_right, h_left:h_right]) * dt / (1000 * t_len)
        print(K.shape, dt, integral_K)
        K_list.append(integral_K)

    plt.xlabel('dt')
    plt.ylabel('Difference')
    K_list = np.array(K_list)
    K_diff = np.abs(K_list - K_list[-1])
    plt.loglog(dt_list[:-1], K_diff[:-1], color='C0', linestyle='--', marker='o')
    # plt.axis('equal')
    plt.show()


dt_convergence()

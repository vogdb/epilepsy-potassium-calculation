from __future__ import division

import matplotlib.pyplot as plt
import numpy as np

from epileptor.model_2d_full import split_h_circle
from epileptor.state_recorder import StateRecorder
from epileptor import util

np.set_printoptions(precision=2, edgeitems=100)


def calc_circle_indices(K, params):
    '''
    Calculates and saves indices of AP circle. Those indices are used
    later to ease profile creation for velocities calculation.

    :param K:
    '''
    _, y_nh, x_nh = K.shape
    in_circle_idx, out_circle_idx = split_h_circle(x_nh, y_nh, params.y_h / y_nh)

    y_list, x_list = in_circle_idx[0], in_circle_idx[1]
    idx_matrix = np.zeros((y_nh, x_nh))
    for i in range(len(x_list)):
        x = x_list[i]
        y = y_list[i]
        idx_matrix[y, x] = y + 0.01 * x
    np.savetxt('velocity_idx_{}_{}.txt'.format(x_nh, y_nh), idx_matrix, fmt='%-6.2f')


def state_image(state, dt):
    '''
    Just takes an image of K at some time to verify that we use correct profile indices.

    :param state:
    '''

    t_i = 16
    t_i_left = int(t_i * 1000 / dt)
    t_i_right = int(t_i_left + 1000 / dt)

    K_value = np.mean(state.values[t_i_left:t_i_right, :, :], axis=0)

    fig = plt.figure(figsize=(12, 8))
    ax = fig.gca()
    img = ax.imshow(K_value, cmap='plasma')
    img.set_clim(state.min, state.max)
    plt.colorbar(mappable=img, ax=ax)
    plt.show()


def calc_time_range_list(K, dt):
    '''
    :param K: values of the shape (time, y, x)
    :return: list of time ranges of a single wave in dt.
    An example is [{'from': 0, 'to': 10000}, {'from': 10000, 'to': 20000}]. 0, 10000, 20000 are dt units.
    '''
    MIN_K_THRESHOLD = 15
    MIN_INTER_WAVE_DISTANCE = util.sec_to_dt(20, dt)
    peak_times = util.get_peak_times(K, MIN_INTER_WAVE_DISTANCE, MIN_K_THRESHOLD)
    if len(peak_times) > 1:
        middle_times = ((peak_times[1:] + peak_times[:-1]) / 2).astype(int)
        interval_times = np.concatenate(([0], middle_times, [K.shape[0]]))
        result = []
        for i in range(len(interval_times) - 1):
            result.append({'from': interval_times[i], 'to': interval_times[i + 1]})
        return result
    elif len(peak_times) == 1:
        return [{'from': 0, 'to': K.shape[0]}]
    else:
        return []


def get_K(filename):
    params, plain, _ = StateRecorder.load('results/{}'.format(filename))
    K = plain['K']
    return params, K


def calc_velocity_from_to(K, from_, to_):
    t_max_from = np.argmax(K[from_], axis=0)
    t_max_to = np.argmax(K[to_], axis=0)
    # start_max_K = np.amax(K[start], axis=0)
    y_from, y_to = from_[1], to_[1]
    dy = abs(y_to - y_from)
    # result is in dh / dt
    t_diff = t_max_to - t_max_from
    return np.divide(dy, t_diff, where=t_diff > 10)


def calc_h_strip_per_file(filename, space_range_list):
    def print_velocity_array(title, velocity_array):
        print('{} Mean: {:.6f}, STD: {:.6f}'.format(
            title,
            np.mean(velocity_array),
            np.std(velocity_array),
        ))

    params, K = get_K(filename)
    dt, y_h = params['dt'], params['y_h']
    dh = y_h / K.shape[1]
    velocity_map = {}
    dt_to_s = dt / 1000

    time_range_list = calc_time_range_list(K, dt)
    if len(time_range_list) == 0:
        print('No waves')
        return
    for time_range in time_range_list:
        for space_range in space_range_list:
            space_from = space_range['from']
            space_to = space_range['to']
            time_from = time_range['from']
            time_to = time_range['to']

            # np.s_[time, y_from, x_where]
            from_ = np.s_[time_from:time_to, space_from[0], space_from[1]]
            # np.s_[time, y_to, x_where]
            to_ = np.s_[time_from:time_to, space_to[0], space_to[1]]
            velocity_dhdt = calc_velocity_from_to(K, from_, to_)
            velocity_dmmds = (dh / dt_to_s) * velocity_dhdt

            space_title = 'space([{}]-{})'.format(space_from, space_to)
            time_title = 'time({:.0f}:{:.0f})'.format(
                dt_to_s * time_from, dt_to_s * time_to
            )
            title = '{}, {}'.format(time_title, space_title)

            velocity_map[title] = velocity_dmmds

    for title, velocity_dmmds in velocity_map.items():
        print_velocity_array('\tVelocity {}.'.format(title), velocity_dmmds)
    print_velocity_array(
        '\n\tGeneral File Velocity.',
        np.array(list(velocity_map.values())).flatten()
    )


def calc_h_strip():
    space_range_list = [
        # format {'from': np.s_[y, x], 'to': np.s_[y, x]}
        # 'from' and 'to' are lines of the same dimensions and parallel to X
        {'from': np.s_[46, 40], 'to': np.s_[66, 40]},
    ]
    file_list = [
        '2019-07-04_17.05',
        # '2019-07-04_18.38',
    ]
    for filename in file_list:
        print('File: {}'.format(filename))
        calc_h_strip_per_file(filename, space_range_list)
        print('\n')


calc_h_strip()
